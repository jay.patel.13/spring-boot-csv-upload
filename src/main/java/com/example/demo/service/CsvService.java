package com.example.demo.service;

import java.util.List;

import org.springframework.web.multipart.MultipartFile;

import com.example.demo.dto.CsvDTO;
import com.example.demo.entities.Csv;

public interface CsvService {

	List<Csv> readFile(MultipartFile file) throws Exception;

	List<Csv> storeFile(MultipartFile file) throws Exception;

	List<CsvDTO> findAllData();

}
