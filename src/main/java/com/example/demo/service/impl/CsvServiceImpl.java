package com.example.demo.service.impl;

import static com.example.demo.commons.AppConstants.NAME;
import static com.example.demo.commons.AppConstants.POSITION;
import static com.example.demo.commons.AppConstants.SALARY;
import static java.lang.Double.parseDouble;

import java.io.InputStreamReader;
import java.io.Reader;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.example.demo.dao.CsvDAO;
import com.example.demo.dto.CsvDTO;
import com.example.demo.entities.Csv;
import com.example.demo.service.CsvService;

@Service
public class CsvServiceImpl implements CsvService {

	@Autowired
	private CsvDAO csvRepository;

	@Override
	public List<Csv> readFile(MultipartFile file) throws Exception {
		List<Csv> data = new ArrayList<>();
		try (Reader reader = new InputStreamReader(file.getInputStream());
				CSVParser csvParser = new CSVParser(reader,
						CSVFormat.DEFAULT.withFirstRecordAsHeader().withIgnoreHeaderCase().withTrim());) {
			for (CSVRecord csvRecord : csvParser) {
				// Accessing values by Header names
				String name = csvRecord.get(NAME);
				String salary = csvRecord.get(SALARY);
				String position = csvRecord.get(POSITION);
				Csv row = new Csv(StringUtils.isNotBlank(salary) ? parseDouble(salary) : 0, name, position);
				data.add(row);
			}
		}
		return data;
	}

	@Override
	public List<Csv> storeFile(MultipartFile file) throws Exception {
		List<Csv> data = this.readFile(file);
		return this.csvRepository.saveAll(data);
	}

	@Override
	public List<CsvDTO> findAllData() {
		List<CsvDTO> csvDTOList = new ArrayList<>();
		List<Csv> csv = this.csvRepository.findAll();
		csv.stream().forEach(obj -> {
			CsvDTO csvDTO = new CsvDTO();
			BeanUtils.copyProperties(obj, csvDTO);
			csvDTOList.add(csvDTO);
		});
		return csvDTOList;
	}

}
