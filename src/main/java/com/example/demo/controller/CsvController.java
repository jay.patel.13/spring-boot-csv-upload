package com.example.demo.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.example.demo.dto.CsvDTO;
import com.example.demo.entities.Csv;
import com.example.demo.service.CsvService;

@RestController
@CrossOrigin
@RequestMapping("/csv")
public class CsvController {

	@Autowired
	private CsvService cs;

	@PostMapping("/uploadFile")
	public List<Csv> uploadFile(@RequestBody MultipartFile file) throws Exception {
		return this.cs.storeFile(file);
	}

	@GetMapping("/getAllData")
	public List<CsvDTO> findAllCityWiseRate() throws Exception {
		return this.cs.findAllData();
	}

}
